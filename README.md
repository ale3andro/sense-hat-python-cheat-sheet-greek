# Sense Hat Python Cheat sheet Greek

To [Sense Hat](https://www.raspberrypi.com/products/sense-hat/) για το Raspberry Pi, είναι εξαιρετικά ενδιαφέρον γιατί ενσωματώνει μια οθόνη LED και πολλούς αισθητήρες. 

Η χρήση του μπορεί να γίνει μέσω του κατάλληλου πρόσθετου στο Scratch 3 (στο Raspberry Pi OS) ενώ μπορεί να χρησιμοποιηθεί και σε Python scripts.

Σε περίπτωση που κάποιος δεν έχει το hat αυτό, μπορεί να χρησιμοποιήσει τον [εξομοιωτή στο trinket](https://trinket.io/sense-hat)

Στο διαδίκτυο υπήρχε ένα πολύ βολικό [cheat sheet](https://github.com/raspberrypilearning/astro-pi-guide/blob/master/files/SenseHAT-Cheatsheet.pdf) με οδηγίες για χρήση του hat μέσω Python, από το [Astro Pi Mission](https://github.com/raspberrypilearning/astro-pi-guide)

Πήρα την πρωτοβουλία και το έκανα την Ελληνική μετάφραση. Ελπίζω να φανεί χρήσιμο.
